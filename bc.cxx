#include <iostream>

#include "constants.hpp"
#include "parameters.hpp"
#include "matprops.hpp"

#include "bc.hpp"


namespace {

void normal_vector_of_facet(int f, const int *conn, const array_t &coord,
                            double *normal, double *tangent, 
                            double &zcenter, double &xcenter, double &ycenter)
{
    int n0 = conn[NODE_OF_FACET[f][0]];
    int n1 = conn[NODE_OF_FACET[f][1]];
#ifdef THREED
    int n2 = conn[NODE_OF_FACET[f][2]];

    // vectors: n0-n1 and n0-n2
    double v01[NDIMS], v02[NDIMS];
    for (int i=0; i<NDIMS; ++i) {
        v01[i] = coord[n1][i] - coord[n0][i];
        v02[i] = coord[n2][i] - coord[n0][i];
    }

    // the outward normal vector is parallel to the cross product
    // of v01 and v02, with magnitude = area of the triangle
    normal[0] = (v01[1] * v02[2] - v01[2] * v02[1]) / 2;
    normal[1] = (v01[2] * v02[0] - v01[0] * v02[2]) / 2;
    normal[2] = (v01[0] * v02[1] - v01[1] * v02[0]) / 2;

    tangent[0] = normal[0];
    tangent[1] = normal[1];
    tangent[2] = normal[2];

    xcenter = (coord[n0][0] + coord[n1][0] + coord[n2][0]) / NODES_PER_FACET;
    ycenter = (coord[n0][1] + coord[n1][1] + coord[n2][1]) / NODES_PER_FACET;
    zcenter = (coord[n0][2] + coord[n1][2] + coord[n2][2]) / NODES_PER_FACET;
#else
    // the edge vector
    double v01[NDIMS];
    for (int i=0; i<NDIMS; ++i) {
        v01[i] = coord[n1][i] - coord[n0][i];
    }

    tangent[0] = v01[0];
    tangent[1] = v01[1];

    // the normal vector to the edge, pointing outward
    normal[0] = v01[1];
    normal[1] = -v01[0];

    xcenter = (coord[n0][0] + coord[n1][0]) / NODES_PER_FACET;
    ycenter = (coord[n0][1] + coord[n1][1]) / NODES_PER_FACET;
    zcenter = ycenter;
#endif
}

}


bool is_on_boundary(const Variables &var, int node)
{
    uint flag = (*var.bcflag)[node];
    return flag & BOUND_ANY;
}

void find_closest_tangent(double &x, const Param &param, const Variables &var, double &TAN0, double &TAN1)
{
    const auto& posish = *var.bedrock_topoXX;
    bool found = false;

    for (std::size_t i=0; i<posish.size(); ++i) {

        double isMinDX = std::abs(posish[i] - x);
        if (isMinDX <= (2.0 * param.mesh.resolution))  {
            TAN0 = (*var.bedrock_topoX)[i];
            TAN1 = (*var.bedrock_topoZ)[i];
//            if (x < 10)
//                std::cerr<<"i="<<i<<" x="<<x<<" topoXX[i]="<<posish[i]<<" TAN="<<TAN0<<" "<<TAN1<<std::endl;
            found = true;
            break;
        }
    }

    if( found == false ) {
        std::cerr<<"Failed to find the closes tangent for x="<<x<<std::endl;
        abort();
    }

}

double find_max_vbc(const BC &bc)
{
    double max_vbc_val = 0;
    if (bc.vbc_x0 % 2 == 1) // odd number indicates fixed velocity component
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_x0));
    if (bc.vbc_x1 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_x1));
    if (bc.vbc_y0 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_y0));
    if (bc.vbc_y1 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_y1));
    if (bc.vbc_z0 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_z0));
    if (bc.vbc_z1 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_z1));
    if (bc.vbc_n0 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_n0));
    if (bc.vbc_n1 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_n1));
    if (bc.vbc_n2 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_n2));
    if (bc.vbc_n3 % 2 == 1)
        max_vbc_val = std::max(max_vbc_val, std::fabs(bc.vbc_val_n3));

    return max_vbc_val;
}


void create_boundary_normals(const Variables &var, double bnormals[nbdrytypes][NDIMS],
                             std::map<std::pair<int,int>, double*>  &edge_vectors)
{
    /* This subroutine finds the outward normal unit vectors of boundaries.
     * There are two types of boundaries: ibound{x,y,z}? and iboundn?.
     * The first type ibound{x,y,z}? has well defined "normal" direction
     * as their name implied. Thus they can be curved. (Ex. the top boundary
     * can have topography.)
     * The second type iboundn?, on the other hand, must be a plane so that
     * their vbc are well defined.
     *
     * If the normal component of the boundary velocity is fixed, the boundary
     * normal will not change with time.
     */

    for (int i=0; i<nbdrytypes; i++) {
        double normal[NDIMS] = {0};
        double tangent[NDIMS] = {0};
        if (var.bfacets[i].size() == 0) continue;

        for (auto j=var.bfacets[i].begin(); j<var.bfacets[i].end(); ++j) {
            int e = j->first;
            int f = j->second;
            double tmp,tmp1,tmp2;
            normal_vector_of_facet(f, (*var.connectivity)[e], *var.coord,
                                   normal, tangent, tmp,tmp1,tmp2);
            // make an unit vector
            double len = 0;
            for(int d=0; d<NDIMS; d++)
                len += normal[d] * normal[d];

            len = std::sqrt(len);
            for(int d=0; d<NDIMS; d++)
                normal[d] = normal[d] / len;

            if (j == var.bfacets[i].begin()) {
                for(int d=0; d<NDIMS; d++)
                    bnormals[i][d] = normal[d];

                if (i < iboundn0) break; // slant boundaries start at iboundn0, other boundary can be curved
            }
            else {
                // Make sure the boundary is a plane, ie. all facets have the same normal vector.
                const double eps2 = 1e-12;
                double diff2 = 0;
                for(int d=0; d<NDIMS; d++)
                    diff2 += (bnormals[i][d] - normal[d]) * (bnormals[i][d] - normal[d]);
                if (diff2 > eps2) {
                    std::cerr << "Error: boundary " << i << " is curved.\n";
                    std::exit(1);
                }
            }
        }
    }

    for (int i=0; i<nbdrytypes; i++) {
        if (var.bfacets[i].size() == 0) continue;

        const double eps = 1e-15;
        for (int j=i+1; j<nbdrytypes; j++) {
            if (var.bfacets[j].size() == 0) continue;
            double *s = new double[NDIMS];  // intersection of two boundaries
                                            // whole-application lifetime, no need to delete manually
#ifdef THREED
            // quick path: both walls are vertical
            if (std::abs(var.bnormals[i][NDIMS-1]) < eps &&
                std::abs(var.bnormals[j][NDIMS-1]) < eps) {
                s[0] = s[1] = 0;
                s[NDIMS-1] = 1;
            }
            else {
                // cross product of 2 normal vectors
                s[0] = var.bnormals[i][1]*var.bnormals[j][2] - var.bnormals[i][2]*var.bnormals[j][1];
                s[1] = var.bnormals[i][2]*var.bnormals[j][0] - var.bnormals[i][0]*var.bnormals[j][2];
                s[2] = var.bnormals[i][0]*var.bnormals[j][1] - var.bnormals[i][1]*var.bnormals[j][0];
            }
#else
            // 2D
            s[0] = 0;
            s[1] = 1;
#endif
            edge_vectors[std::make_pair(i, j)] = s;
        }
    }
}


void apply_vbcs(const Param &param, const Variables &var, array_t &vel)
{
    // meaning of vbc flags (odd: free; even: fixed) --
    // 0: all components free
    // 1: normal component fixed, shear components free
    // 2: normal component free, shear components fixed at 0
    // 3: normal component fixed, shear components fixed at 0
    // 4: normal component free, shear component (not z) fixed, only in 3D
    // 5: normal component fixed at 0, shear component (not z) fixed, only in 3D

    const BC &bc = param.bc;

    // diverging x-boundary
    #pragma omp parallel for default(none) \
        shared(bc, var, vel, param)
    for (int i=0; i<var.nnode; ++i) {

        // fast path: skip nodes not on boundary
        if (! is_on_boundary(var, i)) continue;

        uint flag = (*var.bcflag)[i];
        double *v = vel[i];

        const auto& coord = *var.coord;
        double x0 = coord[i][0];
        double y0 = coord[i][1];
#ifdef THREED
        double z0 = coord[i][2];
#endif

        double TAN0, TAN1; 
        double ang, vs; 

        double waterLevel = 1465;
        
        bool calibrating_ice_plug  = false;


        //
        // X
        //
        if (flag & BOUNDX0) {
            switch (bc.vbc_x0) {
            case 0:
                break;
            case 1:
              if (NDIMS==3) {
                if (y0 >= 168810 - 2.0630*x0 - 25e3) {
                    v[0] = -3e-5*std::cos(.3/.65);
                    if (y0 >= 40e3) {
                    v[1] = -3e-5*std::sin(.3/.65);
                    }
                    else {
                    v[1] = 0;
                    } 
                }
                else if (y0 <= 168810 - 2.0630*x0 -25e3 ) {
                    v[0] = -1e-4*std::cos(.3/.65);
                    v[1] = -1e-4*std::sin(.3/.65);
                    v[2] = 0;
                }
              }
              else if (calibrating_ice_plug){
                  break;
              }
              else {
                  v[0] = bc.vbc_val_x0;
              }
                break;
            case 2:
                v[1] = 0;
#ifdef THREED
                v[2] = 0;
#endif
                break;
            case 3:
                v[0] = bc.vbc_val_x0;
                v[1] = 0;
#ifdef THREED
                v[2] = 0;
#endif
                break;
#ifdef THREED
            case 4:
                v[1] = bc.vbc_val_x0;
                v[2] = 0;
                break;
            case 5:
                v[0] = 0;
                v[1] = bc.vbc_val_x0;
                v[2] = 0;
                break;
            case 7:
                v[0] = bc.vbc_val_x0;
                v[1] = 0;
                break;
#endif
            }
        }
        if (flag & BOUNDX1) {
            switch (bc.vbc_x1) {
            case 0:
                break;
            case 1:
                if (y0 >= waterLevel) { 
                    v[0] = bc.vbc_val_x1;
                }
                break;
            case 2:
                v[1] = 0;
#ifdef THREED
                v[2] = 0;
#endif
                break;
            case 3:
                v[0] = bc.vbc_val_x1;
                v[1] = 0;
#ifdef THREED
                v[2] = 0;
#endif
                break;
#ifdef THREED
            case 4:
                v[1] = bc.vbc_val_x1;
                v[2] = 0;
                break;
            case 5:
                v[0] = 0;
                v[1] = bc.vbc_val_x1;
                v[2] = 0;
                break;
            case 7:
                v[0] = bc.vbc_val_x1;
                v[1] = 0;
                break;
#endif
            }
        }
#ifdef THREED
        //
        // Y
        //
        if (flag & BOUNDY0) {
            switch (bc.vbc_y0) {
            case 0:
                break;
            case 1:
                v[1] = bc.vbc_val_y0;
                break;
            case 2:
                v[0] = 0;
                v[2] = 0;
                break;
            case 3:
                v[0] = 0;
                v[1] = bc.vbc_val_y0;
                v[2] = 0;
                break;
            case 4:
                v[0] = bc.vbc_val_y0;
                v[2] = 0;
                break;
            case 5:
                v[0] = bc.vbc_val_y0;
                v[1] = 0;
                v[2] = 0;
                break;
            case 7:
                v[0] = 0;
                v[1] = bc.vbc_val_y0;
                break;
            }
        }
        if (flag & BOUNDY1) {
            switch (bc.vbc_y1) {
            case 0:
                break;
            case 1:
                v[1] = bc.vbc_val_y1;
                break;
            case 2:
                v[0] = 0;
                v[2] = 0;
                break;
            case 3:
                v[0] = bc.vbc_val_y1;
                v[1] = 0;
                v[2] = 0;
                break;
            case 4:
                v[0] = bc.vbc_val_y1;
                v[2] = 0;
                break;
            case 5:
                v[0] = bc.vbc_val_y1;
                v[1] = 0;
                v[2] = 0;
                break;
            case 7:
                v[0] = 0;
                v[1] = bc.vbc_val_y1;
                break;
            }
        }
#endif

        //
        // N
        //
        for (int ib=iboundn0; ib<=iboundn3; ib++) {
            const double eps = 1e-15;
            const double *n = var.bnormals[ib]; // unit normal vector

            if (flag & (1 << ib)) {
                double fac = 0;
                switch (var.vbc_types[ib]) {
                case 1:
                    if (flag == (1U << ib)) {  // ordinary boundary
                        double vn = 0;
                        for (int d=0; d<NDIMS; d++)
                            vn += v[d] * n[d];  // normal velocity

                        for (int d=0; d<NDIMS; d++)
                            v[d] += (var.vbc_values[ib] - vn) * n[d];  // setting normal velocity
                    }
                    else {  // intersection with another boundary
                        for (int ic=iboundx0; ic<ib; ic++) {
                            if (flag & (1 << ic)) {
                                if (var.vbc_types[ic] == 0) {
                                    double vn = 0;
                                    for (int d=0; d<NDIMS; d++)
                                        vn += v[d] * n[d];  // normal velocity

                                    for (int d=0; d<NDIMS; d++)
                                        v[d] += (var.vbc_values[ib] - vn) * n[d];  // setting normal velocity
                                }
                                else if (var.vbc_types[ic] == 1) {
                                    auto edge = var.edge_vectors.at(std::make_pair(ic, ib));
                                    double ve = 0;
                                    for (int d=0; d<NDIMS; d++)
                                        ve += v[d] * edge[d];

                                    for (int d=0; d<NDIMS; d++)
                                        v[d] = ve * edge[d];  // v must be parallel to edge
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    for (int d=0; d<NDIMS; d++)
                        v[d] = var.vbc_values[ib] * n[d];  // v must be normal to n
                    break;
                case 11:
                    fac = 1 / std::sqrt(1 - n[NDIMS-1]*n[NDIMS-1]);  // factor for horizontal normal unit vector
                    if (flag == (1U << ib)) {  // ordinary boundary
                        double vn = 0;
                        for (int d=0; d<NDIMS-1; d++)
                            vn += v[d] * n[d];  // normal velocity

                        for (int d=0; d<NDIMS-1; d++)
                            v[d] += (var.vbc_values[ib] * fac - vn) * n[d];  // setting normal velocity
                    }
                    else {  // intersection with another boundary
                        for (int ic=iboundx0; ic<ib; ic++) {
                            if (flag & (1 << ic)) {
                                if (var.vbc_types[ic] == 0) {
                                    double vn = 0;
                                    for (int d=0; d<NDIMS-1; d++)
                                        vn += v[d] * n[d];  // normal velocity

                                    for (int d=0; d<NDIMS-1; d++)
                                        v[d] += (var.vbc_values[ib] * fac - vn) * n[d];  // setting normal velocity
                                }
                                else if (var.vbc_types[ic] == 1) {
                                    auto edge = var.edge_vectors.at(std::make_pair(ic, ib));
                                    double ve = 0;
                                    for (int d=0; d<NDIMS; d++)
                                        ve += v[d] * edge[d];

                                    for (int d=0; d<NDIMS; d++)
                                        v[d] = ve * edge[d];  // v must be parallel to edge
                                }
                            }
                        }
                    }
                    break;
                case 13:
                    fac = 1 / std::sqrt(1 - n[NDIMS-1]*n[NDIMS-1]);  // factor for horizontal normal unit vector
                    for (int d=0; d<NDIMS-1; d++)
                        v[d] = var.vbc_values[ib] * fac * n[d];
                    v[NDIMS-1] = 0;
                    break;
                }
            }
        }

        //
        // Z, must be dealt last
        //

        // fast path: vz is usually free in the models
        if (bc.vbc_z0==0 && bc.vbc_z1==0) continue;

        if (flag & BOUNDZ0) {
            switch (bc.vbc_z0) {
            case 0:
                break;
            case 1:
/*                if ( x0 < param.mesh.xlength * .75 & y0 < param.mesh.ylength * .5) {
                    v[0]       = bc.vbc_val_x0;
                    v[NDIMS-1] = bc.vbc_val_z0;
                }*/
                //if (y0 >= 168810 - 2.0630*x0) {
                //}
#ifndef THREED
                if (x0 <= 81e3) {
                    find_closest_tangent(x0, param, var, TAN0, TAN1);
                    ang = std::atan(TAN1/TAN0);
                    vs = bc.vbc_val_x0 * std::exp(3e-5  * x0); // exponential
                    v[0] = vs * std::cos(ang);
                    v[NDIMS-1] = vs * std::sin(ang);
                }
#endif
                break;
            case 2:
                v[0] = 0;
#ifdef THREED
                v[1] = 0;
#endif
                break;
            case 3:
                v[0] = 0;
#ifdef THREED
                v[1] = 0;
#endif
                v[NDIMS-1] = bc.vbc_val_z0;
                break;
            }
        }
        if (flag & BOUNDZ1) {
            switch (bc.vbc_z1) {
            case 0:
                break;
            case 1:
                v[NDIMS-1] = bc.vbc_val_z1;
                break;
            case 2:
                v[0] = 0;
#ifdef THREED
                v[1] = 0;
#endif
                break;
            case 3:
                v[0] = 0;
#ifdef THREED
                v[1] = 0;
#endif
                v[NDIMS-1] = bc.vbc_val_z1;
                break;
            }
        }
    }
}


void apply_stress_bcs(const Param& param, const Variables& var, array_t& force, array_t& vel)
{
    const BC &bc = param.bc;

    /* Various toggles for different situations */
    bool ocean_floatation      = true;
    bool basal_drag            = false;
    bool is_tect_plate_beneath = false;
    bool calibrating_ice_plug  = false;

    if (ocean_floatation) {
        const double seaWaterDensity = 1020;
        double FREQ                  = 2*3.1415/.5/24/3600;
        double tidalHeight           = 1 * std::sin(var.time*FREQ);
#ifdef THREED
        double tonguePos             = 0; 
#else
        double tonguePos             = 1482; 
#endif
        double waterLevel            = (tonguePos + tidalHeight);

        /* Looping over bottom boundaries */
        const int bottom_bdry = iboundz0;
        const auto& bot = var.bfacets[bottom_bdry];

        const auto& coord = *var.coord;
        for (std::size_t i=0; i<bot.size(); ++i) {
            int e = bot[i].first;
            int f = bot[i].second;
            const int *conn = (*var.connectivity)[e];

            double *v = vel[f];
            double speed = std::sqrt(v[0]*v[0] + v[1]*v[1]);

            double normal[NDIMS], tangent[NDIMS];
            double xcenter, ycenter, zcenter;

            normal_vector_of_facet(f, conn, *var.coord, normal, tangent, zcenter, xcenter, ycenter);

            double p = seaWaterDensity * param.control.gravity * (waterLevel - zcenter);

            /* 3D meshes: */
            if (NDIMS==3) {
                if (ycenter <= 168810 - 2.0630*xcenter - 25e3) {
                    for (int j=0; j<NODES_PER_FACET; ++j) {
                        int n = conn[NODE_OF_FACET[f][j]];
                        for (int d=0; d<NDIMS; ++d) {
                            force[n][d] -= p * normal[d] / NODES_PER_FACET;
                        }
                    }
                }
            }
            /* 2D meshes: */
            else {
                /* Normal floatation force */
                if ( xcenter >= 81e3 ) {
                    for (int j=0; j<NODES_PER_FACET; ++j) {
                        int n = conn[NODE_OF_FACET[f][j]];
                        for (int d=0; d<NDIMS; ++d) {
                            force[n][d] -= p * normal[d] / NODES_PER_FACET;
                        }
                    }
                }
                /* Tangential drag force */ 
                else if (basal_drag) {
                    for (int j=0; j<NODES_PER_FACET; ++j) {
                         int n = conn[NODE_OF_FACET[f][j]];
                        for (int d=0; d<NDIMS; ++d) {
                            force[n][d] -= 1e10 * speed * tangent[d] / NODES_PER_FACET;
                        }
                    } 
                }
            }
        } /* End of bottom boundary loop */

#ifdef THREED
        const int side_bdry = iboundx0;
#else
        const int side_bdry = iboundx1;
#endif
        const auto& side = var.bfacets[side_bdry];

        /* Loop over side, sea-touching boundary */
        for (std::size_t i=0; i<side.size(); ++i) {
            int e = side[i].first;
            int f = side[i].second;
            const int *conn = (*var.connectivity)[e];

            double normal[NDIMS], tangent[NDIMS];
            double xcenter, ycenter, zcenter;

            normal_vector_of_facet(f, conn, *var.coord, normal, tangent, zcenter, xcenter, ycenter);

            double p = seaWaterDensity * param.control.gravity * (waterLevel - zcenter);

            if (NDIMS==3) {
                if (ycenter <= 168810 - 2.0630*xcenter - 25e3) {
                    for (int j=0; j<NODES_PER_FACET; ++j) {
                        int n = conn[NODE_OF_FACET[f][j]];
                        for (int d=0; d<NDIMS; ++d) {
                            force[n][d] -= p * normal[d] / NODES_PER_FACET;
                        }
                    }
                }
            }
            else {
                if ( xcenter >= 81e3 ) {
                    for (int j=0; j<NODES_PER_FACET; ++j) {
                        int n = conn[NODE_OF_FACET[f][j]];
                        for (int d=0; d<NDIMS; ++d) {
                            force[n][d] -= p * normal[d] / NODES_PER_FACET;
                        }
                    }
                }
            }
        }
     } /* End if-statement on floatation stress */

/* Elastic base force */
    if (is_tect_plate_beneath) {
        if (NDIMS==3 && param.bc.has_elastic_foundation) {
            /* A restoration force on the bottom nodes proportional to total vertical displacement */
            for (auto i=var.bnodes[iboundz0].begin(); i<var.bnodes[iboundz0].end(); ++i) {
                int n = *i;
                if ((*var.coord)[n][NDIMS-2] >= 168810 - 2.0630*(*var.coord)[n][0] - 25e3) { 
                    force[n][NDIMS-1] -= param.bc.elastic_foundation_constant * ((*var.coord)[n][NDIMS-1] - (*var.coord0)[n][NDIMS-1]);
                }
            }
        }
        else if (NDIMS==2 && param.bc.has_elastic_foundation) {
            /* A restoration force on the bottom nodes proportional to total vertical displacement */
            for (auto i=var.bnodes[iboundz0].begin(); i<var.bnodes[iboundz0].end(); ++i) {
                int n = *i;
    	    if ( (*var.coord)[n][0] <= 81e3 ) {
                    force[n][NDIMS-1] -= param.bc.elastic_foundation_constant * ((*var.coord)[n][NDIMS-1] - (*var.coord0)[n][NDIMS-1]);
                }
            }
        }
    }

/* Ice plug calibration study */
    if (calibrating_ice_plug) {
        const int top_bdry = iboundz1;
        const auto& top = var.bfacets[top_bdry];

        for (std::size_t i=0; i<top.size(); ++i) {
            int e = top[i].first;
            int f = top[i].second;
            const int *conn = (*var.connectivity)[e];

            double normal[NDIMS], tangent[NDIMS];
            double xcenter, ycenter, zcenter;

            normal_vector_of_facet(f, conn, *var.coord, normal, tangent, zcenter, xcenter, ycenter);

            double p = 930000; // {.64, .82, .93} MPa 

            for (int j=0; j<NODES_PER_FACET; ++j) {
                int n = conn[NODE_OF_FACET[f][j]];
                for (int d=0; d<NDIMS; ++d) {
                    force[n][d] -= p * normal[d] / NODES_PER_FACET;
                }
            }
        }
    }
}
/* End of apply_stress_bcs() */

void bottom_side_Thwaites(const Param &param, const Variables &var, double_vec& bedrock_topoXX, double_vec& bedrock_topoX, double_vec& bedrock_topoZ)
{

    const BC &bc = param.bc;
    const int bottom_bdry = iboundz0;
    const auto& bot = var.bfacets[bottom_bdry];
    const auto& coord = *var.coord;

    double* XX = bedrock_topoXX.data();
    double* X = bedrock_topoX.data();
    double* Z = bedrock_topoZ.data();

    for (std::size_t i=0; i<bot.size(); ++i) {
        int e = bot[i].first;
        int f = bot[i].second;
        const int *conn = (*var.connectivity)[e];

        double normal[NDIMS], tangent[NDIMS];
        double xcenter, zcenter, ycenter;

        normal_vector_of_facet(f, conn, *var.coord, normal, tangent, zcenter, xcenter, ycenter);

        XX[i] = xcenter;
        X[i] = tangent[0];
        Z[i] = tangent[1];

    }

}

namespace {

    void simple_diffusion(const Variables& var, array_t& coord,
                          double surface_diffusivity)
    {
        /* Diffusing surface topography to simulate the effect of erosion and
         * sedimentation.
         */

        const int top_bdry = iboundz1;
        const auto& top = var.bfacets[top_bdry];

        const int_vec& top_nodes = var.bnodes[top_bdry];
        const std::size_t ntop = top_nodes.size();
        double_vec total_dx(var.nnode, 0);
        double_vec total_slope(var.nnode, 0);

        // loops over all top facets
        for (std::size_t i=0; i<top.size(); ++i) {
            // this facet belongs to element e
            int e = top[i].first;
            // this facet is the f-th facet of e
            int f = top[i].second;

            const int *conn = (*var.connectivity)[e];
            int n0 = (*var.connectivity)[e][NODE_OF_FACET[f][0]];
            int n1 = (*var.connectivity)[e][NODE_OF_FACET[f][1]];

#ifdef THREED
            int n2 = (*var.connectivity)[e][NODE_OF_FACET[f][2]];

            double projected_area;
            {
                // normal vector of this facet
                double normal[NDIMS];

                // two vectors n0-n1 and n0-n2
                // n is the cross product of these two vectors
                // the length of n is 2 * triangle area
                double x01, y01, z01, x02, y02, z02;
                x01 = coord[n1][0] - coord[n0][0];
                y01 = coord[n1][1] - coord[n0][1];
                z01 = coord[n1][2] - coord[n0][2];
                x02 = coord[n2][0] - coord[n0][0];
                y02 = coord[n2][1] - coord[n0][1];
                z02 = coord[n2][2] - coord[n0][2];

                normal[0] = y01*z02 - z01*y02;
                normal[1] = z01*x02 - x01*z02;
                normal[2] = x01*y02 - y01*x02;

                /* the area of this facet is:
                 *   0.5 * std::sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2])
                 *
                 * theta is the angle between this facet and horizontal
                 *   tan_theta = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]) / normal[2]
                 *   cos_theta = normal[2] / (2 * area)
                 *
                 * the area projected on the horizontal plane is:
                 *   projected_area = area * cos_theta = 0.5 * normal[2]
                 */
                projected_area = 0.5 * normal[2];
            }

            total_dx[n0] += projected_area;
            total_dx[n1] += projected_area;
            total_dx[n2] += projected_area;

            double shp2dx[NODES_PER_FACET], shp2dy[NODES_PER_FACET];
            double iv = 1 / (2 * projected_area);
            shp2dx[0] = iv * (coord[n1][1] - coord[n2][1]);
            shp2dx[1] = iv * (coord[n2][1] - coord[n0][1]);
            shp2dx[2] = iv * (coord[n0][1] - coord[n1][1]);
            shp2dy[0] = iv * (coord[n2][0] - coord[n1][0]);
            shp2dy[1] = iv * (coord[n0][0] - coord[n2][0]);
            shp2dy[2] = iv * (coord[n1][0] - coord[n0][0]);

            double D[NODES_PER_FACET][NODES_PER_FACET];
            for (int j=0; j<NODES_PER_FACET; j++) {
                for (int k=0; k<NODES_PER_FACET; k++) {
                    D[j][k] = (shp2dx[j] * shp2dx[k] +
                               shp2dy[j] * shp2dy[k]);
                }
            }

            const int n[NODES_PER_FACET] = {n0, n1, n2};
            for (int j=0; j<NODES_PER_FACET; j++) {
                double slope = 0;
                for (int k=0; k<NODES_PER_FACET; k++)
                    slope += D[j][k] * coord[n[k]][2];

                total_slope[n[j]] += slope * projected_area;
            }

            // std::cout << i << ' ' << n0 << ' ' << n1 << ' ' << n2 << "  "
            //           << projected_area << "  " << slope << '\n';
#else
            /* The 1D diffusion operation is implemented ad hoc, not using FEM
             * formulation (e.g. computing shape function derivation on the edges).
             */

            double dx = std::fabs(coord[n1][0] - coord[n0][0]);
            total_dx[n0] += dx;
            total_dx[n1] += dx;

            double slope = (coord[n1][1] - coord[n0][1]) / dx;
            total_slope[n0] -= slope;
            total_slope[n1] += slope;

            // std::cout << i << ' ' << n0 << ' ' << n1 << "  " << dx << "  " << slope << '\n';
#endif
        }

        double max_dh = 0;
        for (std::size_t i=0; i<ntop; ++i) {
            // we don't treat edge nodes specially, i.e. reflecting bc is used for erosion.
            int n = top_nodes[i];
            double dh = surface_diffusivity * var.dt * total_slope[n] / total_dx[n];
            coord[n][NDIMS-1] -= dh;
            max_dh = std::max(max_dh, std::fabs(dh));
            // std::cout << n << "  dh:  " << dh << '\n';
        }

        // std::cout << "max erosion / sedimentation rate (cm/yr):  "
        //           << max_dh / var.dt * 100 * YEAR2SEC << '\n';
    }


    void custom_surface_processes(const Variables& var, array_t& coord, array_t& vel)
    {
        const int bot_bdry = iboundz0;
        const int_vec& bot_nodes = var.bnodes[bot_bdry];
        const std::size_t nbot = bot_nodes.size();

        // This is where basal melting is done:
        // loops over all bot nodes
        for (std::size_t i=0; i<nbot; ++i) {
            int n = bot_nodes[i];

            // coordinate of this node
            double x = coord[n][0];
            double z = coord[n][NDIMS-1];

            double dh;

            if (x > 81e3) {
                dh = (100.0/(365.0*24.0*3600.0)) * var.dt;
                coord[n][NDIMS-1] += dh;
                //std::cout << x << "  dh:  " << dh << '\n';
            }
        }

        const int top_bdry = iboundz1;
        const int_vec& top_nodes = var.bnodes[top_bdry];
        const std::size_t ntop = top_nodes.size();

        // loops over all top nodes
/*        for (std::size_t i=0; i<ntop; ++i) {
            int n = top_nodes[i];

            double *v = vel[i];

            // coordinate of this node
            double x = coord[n][0];
            double z = coord[n][NDIMS-1];

            double dh, v_tot, v_avg; 

            double one_week = 1*7*24*3600;

            if (var.time % one_week == 0) {
                v_avg = v_tot/one_week;
            }

            // compute topography changes here ...
            // positive dh means sedimentation, negative dh means erosion
            if (x < 81e3) {
                dh = -1.0 * v_avg * var.dt;
            }
            coord[n][NDIMS-1] += dh;
        }
*/
    }

}


void surface_processes(const Param& param, const Variables& var, array_t& coord, array_t& vel)
{
    switch (param.control.surface_process_option) {
    case 0:
        // no surface process
        break;
    case 1:
        simple_diffusion(var, coord, param.control.surface_diffusivity);
        break;
    case 101:
        custom_surface_processes(var, coord, vel);
        break;
    default:
        std::cout << "Error: unknown surface process option: " << param.control.surface_process_option << '\n';
        std::exit(1);
    }
}


